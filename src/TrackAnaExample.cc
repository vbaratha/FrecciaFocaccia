#include "TrackAnaExample.h"
#include "geo.h"

#include <TDirectory.h>
#include <TProfile.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TProfile2D.h>

#include <iostream>
#include <cmath>

class TrackAnaExample_impl {
public:
  TrackAnaExample_impl();
  ~TrackAnaExample_impl(){};
  void write();
  std::map<unsigned, unsigned> n_tracks;
  std::map<unsigned, unsigned> n_hits;
  std::map<unsigned, std::shared_ptr<TH2F> > folded_map_total;
  std::map<unsigned, std::shared_ptr<TH2F> > folded_map_size0;
  std::map<unsigned, std::shared_ptr<TH2F> > folded_map_size1;
  std::map<unsigned, std::shared_ptr<TH2F> > folded_map_size2;
  std::map<unsigned, std::shared_ptr<TH2F> > folded_map_size3;
  std::map<unsigned, std::shared_ptr<TH2F> > folded_map_size4;
  std::map<unsigned, std::shared_ptr<TProfile2D> > folded_map_ToT;
  
  std::map<unsigned, std::shared_ptr<TH1F> > trkcol;
  std::map<unsigned, std::shared_ptr<TH1F> > trkrow;
  
};


//____________________________________________________________________________________________________
TrackAnaExample_impl::TrackAnaExample_impl() {
  for( auto& plane : geo::planes ) {
    
    n_tracks[plane] = 0;
    n_hits[plane]   = 0;

    const auto& geom = geo::geometry[plane];

    const double xmin = -geom.col_width()/2.0;
    const double xmax =  geom.col_width()/2.0;
    const double ymin = -geom.row_width()/2.0;
    const double ymax =  geom.row_width()/2.0;
    
    folded_map_total.emplace( plane, std::make_shared<TH2F>(Form("folded_map_total_pl%u", plane), ";x [mm];y [mm];Entries", 50, xmin, xmax, 50, ymin, ymax) );
    folded_map_size0.emplace( plane, std::make_shared<TH2F>(Form("folded_map_size0_pl%u", plane), ";x [mm];y [mm];Entries", 50, xmin, xmax, 50, ymin, ymax) );
    folded_map_size1.emplace( plane, std::make_shared<TH2F>(Form("folded_map_size1_pl%u", plane), ";x [mm];y [mm];Entries", 50, xmin, xmax, 50, ymin, ymax) );
    folded_map_size2.emplace( plane, std::make_shared<TH2F>(Form("folded_map_size2_pl%u", plane), ";x [mm];y [mm];Entries", 50, xmin, xmax, 50, ymin, ymax) );
    folded_map_size3.emplace( plane, std::make_shared<TH2F>(Form("folded_map_size3_pl%u", plane), ";x [mm];y [mm];Entries", 50, xmin, xmax, 50, ymin, ymax) );
    folded_map_size4.emplace( plane, std::make_shared<TH2F>(Form("folded_map_size4_pl%u", plane), ";x [mm];y [mm];Entries", 50, xmin, xmax, 50, ymin, ymax) );
    
    folded_map_ToT.emplace( plane, std::make_shared<TProfile2D>(Form("folded_map_ToT_pl%u", plane), ";x [mm];y [mm];Average ToT", 50, xmin, xmax, 50, ymin, ymax) );
  }
}


//____________________________________________________________________________________________________
void TrackAnaExample_impl::write() {
  std::cout << "\n--------------------------------------------------\n" << std::endl;
  std::cout << "[ROI efficiency]" << std::endl;
  for( const auto& plane : geo::planes ) {
    if( geo::geometry[plane].plane_type != "dut" ) continue;

    double eff     = (n_hits[ plane ]*1.0)/(n_tracks[ plane ]*1.0);
    double eff_err = sqrt( eff*(1.0-eff)/( n_tracks[plane] * 1.0 ) );
    
    std::cout << Form("Plane %2d: %8d %8d --> eff = %.4f +/- %.4f", plane, n_tracks[ plane ], n_hits[ plane ], eff, eff_err) << std::endl;
  }

  TFile *ofile = new TFile("output/result.root", "recreate");
  for( auto& plane : geo::planes ) {
    ofile->cd();
    gDirectory->mkdir(Form("plane%u", plane))->cd();
    folded_map_total[plane]->Write();
    folded_map_size0[plane]->Write();
    folded_map_size1[plane]->Write();
    folded_map_size2[plane]->Write();
    folded_map_size3[plane]->Write();
    folded_map_size4[plane]->Write();
    
    folded_map_ToT[plane]->Write();
    
  }
  ofile->Close();
}


//____________________________________________________________________________________________________
TrackAnaExample::TrackAnaExample( const config& c, const std::string& fname, const unsigned& id )
  : TrackAnalysis( c, "TrackAnaExample", id, fname )
  , m_impl( new TrackAnaExample_impl )
{}

TrackAnaExample::~TrackAnaExample(){}

//____________________________________________________________________________________________________
void TrackAnaExample::init() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;

  file = std::unique_ptr<TFile>( TFile::Open( filename.c_str() ) );
  if( !file->IsOpen() ) throw( "File is not open!" );
  tree = dynamic_cast<TTree*>( file->Get("trackTree") );
  if(!tree) throw( "tree is not available!" );

  initTree();

  file->cd();
}

//____________________________________________________________________________________________________
void TrackAnaExample::processEntry() {

  // Some general track requirements
  if( vars.chi2 > 10.0 ) return;
  if( vars.nhits_tel < 5 ) return;

  // Loop over planes
  for( unsigned ipl = 0; ipl < vars.plane->size(); ipl ++ ) {
    
    const auto& d = vars.data( ipl );
    
    if( d.x < -5.0 or d.x > 4.0 ) continue;
    if( d.y < -3.0 or d.y > 1.0 ) continue;

    const auto& geom = geo::geometry[d.plane];

    // Reject the track if the entering pixel is not read out
    if( !geom.is_readout( d.trkcol, d.trkrow ) ) continue;

    m_impl->folded_map_total[d.plane]->Fill( d.folded_x, d.folded_y );
    
    switch( d.size ) {
    case 0:
      m_impl->folded_map_size0[d.plane]->Fill( d.folded_x, d.folded_y ); break;
    case 1:
      m_impl->folded_map_size1[d.plane]->Fill( d.folded_x, d.folded_y ); break;
    case 2:
      m_impl->folded_map_size2[d.plane]->Fill( d.folded_x, d.folded_y ); break;
    case 3:
      m_impl->folded_map_size3[d.plane]->Fill( d.folded_x, d.folded_y ); break;
    case 4:
      m_impl->folded_map_size4[d.plane]->Fill( d.folded_x, d.folded_y ); break;
    }

    if( d.size > 0 ) {
      m_impl->folded_map_ToT[d.plane]->Fill( d.folded_x, d.folded_y, d.sumToT );
    }
    
    // Reject the track if the position is out of region of interest
    if( !geom.is_roi( d.folded_x, d.folded_y ) ) continue;

    m_impl->n_tracks[ d.plane ]++;

    if( d.is_hit ) {
      m_impl->n_hits[ d.plane ]++;
    }
    
  }
  
}

void TrackAnaExample::end() {
  m_impl->write();
}
 
