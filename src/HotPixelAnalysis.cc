#include "HotPixelAnalysis.h"
#include "geo.h"

class HotPixelAnalysis_impl {
public:
  // Holder of hit maps
  std::map<int, IHitMap*> hitmaps;
  std::map<int, TH2F*> maskmaps;

  unsigned long long nEvents;
  
  HotPixelAnalysis_impl() : nEvents(0) {};
  
  ~HotPixelAnalysis_impl(){
    for( auto& pair : hitmaps )  delete  pair.second;
    for( auto& pair : maskmaps ) delete pair.second;
  };
  
};



TH2F *gen_noisemask( TH2F *hmap, const double threshold ) {
  auto *maskmap = dynamic_cast<TH2F*>( hmap->Clone( Form("%s_maskmap", hmap->GetName()) ) );
  maskmap->Reset();
  for(int xbin=0; xbin<hmap->GetNbinsX(); xbin++) {
    for(int ybin=0; ybin<hmap->GetNbinsY(); ybin++) {
      const double& content = hmap->GetBinContent(xbin+1, ybin+1);
      if( content > threshold ) maskmap->Fill( xbin+1, ybin+1 );
    }}
  return maskmap;
}




HotPixelAnalysis::HotPixelAnalysis( const config& c, const std::string& _filename, const unsigned& num )
  : RawAnalysis( c, "HotPixelAnalysis", num, _filename )
  , m_impl( new HotPixelAnalysis_impl )
{}

HotPixelAnalysis::~HotPixelAnalysis(){
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}




void HotPixelAnalysis::init() {


  initTree();

  // telescope hit maps
  for( const auto& plane : geo::planes ) {

    const auto& geom = geo::geometry[plane];

    const std::string name = Form("hitmap_pl%u", plane );

    if( geom.sensor_name == "mimosa26"   ) { m_impl->hitmaps[plane] = new HitMap<FE::mimosa26>   ( name.c_str() ); }
    if( geom.sensor_name == "fei4"       ) { m_impl->hitmaps[plane] = new HitMap<FE::fei4>       ( name.c_str() ); }
    if( geom.sensor_name == "fei4_50x50" ) { m_impl->hitmaps[plane] = new HitMap<FE::fei4_50x50> ( name.c_str() ); }
    if( geom.sensor_name == "fei4_100x25") { m_impl->hitmaps[plane] = new HitMap<FE::fei4_100x25>( name.c_str() ); }
  }
}

void HotPixelAnalysis::processEntry() {
  
  for(auto ihit=0; ihit<data.size(); ihit++) {
    auto* hit = data.get_hit( ihit );
    
    dynamic_cast<TH2F*>( m_impl->hitmaps[ hit->iden ] )->Fill( hit->col, hit->row );
  }
  m_impl->nEvents++;
}

void HotPixelAnalysis::end() {
  
  for( auto& pair : m_impl->hitmaps ) {
    auto* m = dynamic_cast<TH2F*>( pair.second );
    const auto& geom = geo::geometry[pair.first];
    
    const double thr = cfg[ana_type][order][ana_name]["threshold"][geom.plane_type];
    
    m_impl->maskmaps[ pair.first ] = gen_noisemask( m, thr*( m_impl->nEvents ) );
  }

  std::string ofilename = cfg[ana_type][order][ana_name]["output"];
  TFile *ofile = new TFile( ofilename.c_str(), "recreate");
  for( auto pair : m_impl->hitmaps ) {
    auto* m = pair.second;
    dynamic_cast<TH2F*>( m )->Scale( 1.0/(m_impl->nEvents) );
    dynamic_cast<TH2F*>( m )->Write();
    m->get_distribution( (m_impl->nEvents) )->Write();
    m->get_distribution_log( (m_impl->nEvents)/100 )->Write();
  }
  for( auto& pair : m_impl->maskmaps ) {
    auto* m = pair.second;
    m->Write();
  }
  ofile->Close();
  
}


