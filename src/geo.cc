#include "geo.h"
#include "Config.h"
#include <TVector2.h>
#include <fstream>

std::vector<unsigned> geo::planes;
std::unordered_map<unsigned, geo> geo::geometry;

geo::geo() 
  : position    ( nullptr )
  , uncertainty ( nullptr )
  , col_width   ( nullptr )
  , row_width   ( nullptr )
  , sensor_col  ( nullptr )
  , sensor_row  ( nullptr )
  , is_readout  ( nullptr )
  , is_roi      ( nullptr )
  , is_toFlip   ( nullptr )
  , sensor_name ( "undefined" )
  , plane_type  ( "undefined" )
  , fliprot     ( { 1, 0, 0, 1 } )
  , pre_align   ( 0, 0 )
  , Trans       ( 0, 0, 0 )
  , Rot         ( 0, 0, 0 )
  , Scale       ( 0.0, 0.0 )
  , Trans_err   ( 0, 0, 0 )
  , Rot_err     ( 0, 0, 0 )
  , Scale_err   ( 0.0, 0.0 )
  , error_scale ( 1.0, 1.0 )
{}

geo::geo( const std::string&name, SensorGeo::posfuncs funcs, std::string& type, const double& z, std::array<double, 4> matrix)
  : position    ( std::get<SensorGeo::func::position>    ( funcs ) )
  , uncertainty ( std::get<SensorGeo::func::uncertainty> ( funcs ) )
  , ncol        ( std::get<SensorGeo::func::ncol>        ( funcs ) )
  , nrow        ( std::get<SensorGeo::func::nrow>        ( funcs ) )
  , col_width   ( std::get<SensorGeo::func::wcol>        ( funcs ) )
  , row_width   ( std::get<SensorGeo::func::wrow>        ( funcs ) )
  , sensor_col  ( std::get<SensorGeo::func::conv_col>    ( funcs ) )
  , sensor_row  ( std::get<SensorGeo::func::conv_row>    ( funcs ) )
  , is_readout  ( std::get<SensorGeo::func::readout>     ( funcs ) )
  , is_roi      ( std::get<SensorGeo::func::roi>         ( funcs ) )
  , is_toFlip   ( std::get<SensorGeo::func::flip>        ( funcs ) )
  , sensor_name ( name )
  , plane_type  ( type )
  , fliprot     ( matrix )
  , pre_align   ( 0, 0 )
  , Trans       ( 0, 0, z )
  , Rot         ( 0, 0, 0 )
  , Scale       ( 0.0, 0.0 )
  , Trans_err   ( 0, 0, 0 )
  , Rot_err     ( 0, 0, 0 )
  , Scale_err   ( 0.0, 0.0 )
  , error_scale ( 1.0, 1.0 )
{}

void geo::init( const config& cfg ) {
  if( planes.size() != 0 ) return;
  
  using funcs = SensorGeo::posfuncs(*)();
  std::map<std::string, funcs> fe_map;
  fe_map["mimosa26"]    = &SensorGeo::funcs<FE::mimosa26>;
  fe_map["fei4"]        = &SensorGeo::funcs<FE::fei4>;
  fe_map["fei4_50x50"]  = &SensorGeo::funcs<FE::fei4_50x50>;
  fe_map["fei4_100x25"] = &SensorGeo::funcs<FE::fei4_100x25>;
  
  if( cfg.find("geometry") != cfg.end () ) {
    auto& cfg_geo = cfg["geometry"];
    
    for( auto it = cfg_geo.begin(); it != cfg_geo.end(); ++it ) {
      auto& geom  = it.value();
      
      auto& plane      = geom["plane"];
      auto& fe_type    = geom["FE"];
      auto& zpos       = geom["zpos"];
      std::string type = geom["type"];
      
      /*std::cout << "loading geometry for plane " << plane << std::endl;*/
      
      planes.emplace_back( plane );
      
      std::array<double, 4> fliprot;
      for( unsigned i=0; i<geom["fliprot"].size(); i++ ) {
        fliprot[i] = geom["fliprot"][i];
      }
      
      geo::geometry[plane] = geo( fe_type, fe_map[fe_type](), type, zpos, fliprot );
      geo::geometry[plane].error_scale = TVector2( geom["error_scale"][0], geom["error_scale"][1] );
    }
  }
  
}

void geo::load_alignment( const config& cfg ) {
  const std::string filename = cfg["alignment"]["output"];
  std::ifstream config_file( filename.c_str() );
  config align_cfg;
  config_file >> align_cfg;
  config_file.close();
  
  if( align_cfg.find("geometry_fine_alignment") != align_cfg.end () ) {
    auto& align_cfg_align = align_cfg["geometry_fine_alignment"];
    
    for( auto it = align_cfg_align.begin(); it != align_cfg_align.end(); ++it ) {
      const auto plane  = std::stoi( it.key() );
      const auto& align  = it.value();
      
      auto& geom = geometry[plane];
      if( align.find("pre_align") != align.end() ) 
        geom.pre_align.Set( static_cast<double>(align["pre_align"][0]), static_cast<double>(align["pre_align"][1]) );
      
      if( align.find("trans") != align.end() ) 
        geom.Trans.SetXYZ( align["trans"][0], align["trans"][1], align["trans"][2] );
      
      if( align.find("rot") != align.end() )
        geom.Rot.SetXYZ( align["rot"][0], align["rot"][1], align["rot"][2] );
      
      if( align.find("scale") != align.end() )
        geom.Scale.Set( static_cast<double>(align["scale"][0]), static_cast<double>(align["scale"][1]) );
      
      if( align.find("error_scale") != align.end() )
        geom.error_scale.Set( static_cast<double>(align["error_scale"][0]), static_cast<double>(align["error_scale"][1]) );
    }
  }
  
  std::cout << Form("Alignment constant %s loaded", filename.c_str() ) << std::endl;
}

TVector2 geo::apply_fliprot( const TVector2& vpos ) const {
  return TVector2( fliprot[0]*vpos.X() + fliprot[1]*vpos.Y(), fliprot[2]*vpos.X() + fliprot[3]*vpos.Y() );
}

TVector2 geo::apply_fliprot_inv( const TVector2& vpos ) const {
  const double det = fliprot[0]*fliprot[3] - fliprot[1]*fliprot[2];
  return TVector2( (fliprot[3]*vpos.X() - fliprot[1]*vpos.Y())/det, (-fliprot[2]*vpos.X() + fliprot[0]*vpos.Y())/det );
}

void geo::apply_alignment( TVector3 &vpos ) const {
  vpos.SetXYZ( vpos.x() * (1.0 + Scale.X() ), vpos.y() * (1.0 + Scale.Y() ), vpos.z() - Trans.z() );
  vpos.RotateX( Rot.x() );
  vpos.RotateY( Rot.y() );
  vpos.RotateZ( Rot.z() );
  vpos += Trans;
  return;
}

void geo::apply_alignment_inv( TVector3 &vpos ) const {
  vpos -= Trans;
  vpos.RotateZ( -Rot.z() );
  vpos.RotateY( -Rot.y() );
  vpos.RotateX( -Rot.x() );
  vpos.SetXYZ( vpos.x() / (1.0 + Scale.X() ), vpos.y() / (1.0 + Scale.Y() ), vpos.z() + Trans.z() );
  return;
}

TVector3 geo::pixel_to_global( const double& col, const double& row, prealign do_prealign, align do_align ) const {
  // Get the bare sensor-internal position
  const auto& vpos = position( col, row );
  
  // Initial flip/rot
  const TVector2& vpos_flipped = apply_fliprot( vpos );
  
  const TVector2& vpos_prealigned = ( do_prealign == prealign::with_prealign) ? (vpos_flipped - pre_align) : vpos_flipped;
  
  TVector3 gpos( vpos_prealigned.X(), vpos_prealigned.Y(), Trans.z() );
  
  // Apply alignment corrections
  if( do_align == align::with_align ) {
    apply_alignment( gpos );
  }
  
  return gpos;
  
}

TVector2 geo::pixel_to_local( const double& col, const double& row ) const {
  
  // Get the bare sensor-internal position
  return position( col, row );
  
}

TVector2 geo::global_to_local( const TVector3& global_pos ) const {
  TVector3 v = global_pos;
  
  apply_alignment_inv( v );
  
  //std::cout << __PRETTY_FUNCTION__ << Form(": after reverting align: (%.3f, %.3f, %.3f)", v.x(), v.y(), v.z() ) << std::endl;
  
  TVector2 v2( v.x(), v.y() );
  v2 += pre_align;
  
  v2 = apply_fliprot_inv( v2 );
  return v2;
}


TVector3 geo::local_to_global( const TVector2& local_pos, align do_align ) const {
  // Initial flip/rot
  const TVector2& vpos_flipped = apply_fliprot( local_pos );
  
  TVector2 vpos_prealigned = vpos_flipped - pre_align;
  
  TVector3 gpos( vpos_prealigned.X(), vpos_prealigned.Y(), Trans.z() );
  
  // Apply alignment corrections
  if( do_align == align::with_align ) {
    apply_alignment( gpos );
  }
  
  return gpos;
  
}


TVector3 geo::global_err() const {
  
  // Get the bare sensor-internal position
  TVector2 vpos = uncertainty();
  vpos.Set( vpos.X()*error_scale.X(), vpos.Y()*error_scale.Y() );
  
  // Initial flip/rot
  const TVector2& vpos_flipped = apply_fliprot( vpos );
  
  TVector3 gpos( fabs(vpos_flipped.X()), fabs(vpos_flipped.Y()), 0 );
  
  return gpos;
  
}

TVector3 geo::local_err() const {
  // Get the bare sensor-internal position
  const TVector2& vpos = uncertainty();
  
  return TVector3( vpos.X()*error_scale.X(), vpos.Y()*error_scale.Y(), 0 );
}

TVector2 geo::local_to_folded( const TVector2& local, const int& col, const int& row, flip do_flip ) const {
  TVector2 trkpixel_pos = pixel_to_local( static_cast<double>(col), static_cast<double>(row) );
  TVector2 folded = local - trkpixel_pos;
  if( do_flip == flip::with_flip ) {
    if( is_toFlip( col ) ) folded.Set( -folded.X(), folded.Y() );
  }
  
  return folded;
}

void geo::get_pixel( TVector2& local_pos, int& col, int& row ) const {
  const double& x = local_pos.X();
  const double& y = local_pos.Y();
  
  col = static_cast<int>( std::floor( x/col_width() ) ) + ncol()/2;
  row = static_cast<int>( std::floor( y/row_width() ) ) + nrow()/2;
  return;
}

