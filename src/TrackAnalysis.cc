#include "TrackAnalysis.h"
#include "geo.h"

#include <iostream>
#include <cmath>

//____________________________________________________________________________________________________
TrackAnalysis::TrackAnalysis( const config& c, const std::string& name, const unsigned& id, const std::string& fname )
  : IAnalysis( c, name, "TrackAnalysis", id )
  , filename( fname )
{}

TrackAnalysis::~TrackAnalysis(){}

//____________________________________________________________________________________________________
void TrackAnalysis::initTree() {
  file = std::unique_ptr<TFile>( TFile::Open( filename.c_str() ) );
  if( !file->IsOpen() ) throw( "File is not open!" );
  tree = dynamic_cast<TTree*>( file->Get("trackTree") );
  if(!tree) throw( "tree is not available!" );
  
  tree->SetBranchAddress("plane",     &(vars.plane)     );
  tree->SetBranchAddress("nhits",     &(vars.nhits)     );
  tree->SetBranchAddress("nhits_tel", &(vars.nhits_tel) );
  tree->SetBranchAddress("size",      &(vars.size)      );
  tree->SetBranchAddress("sumToT",    &(vars.sumToT)    );
  tree->SetBranchAddress("x",         &(vars.x)         );
  tree->SetBranchAddress("y",         &(vars.y)         );
  tree->SetBranchAddress("folded_x",  &(vars.folded_x)  );
  tree->SetBranchAddress("folded_y",  &(vars.folded_y)  );
  tree->SetBranchAddress("local_x",   &(vars.local_x)   );
  tree->SetBranchAddress("local_y",   &(vars.local_y)   );
  tree->SetBranchAddress("trkcol",    &(vars.trkcol)    );
  tree->SetBranchAddress("trkrow",    &(vars.trkrow)    );
  tree->SetBranchAddress("ave_row",   &(vars.ave_row)   );
  tree->SetBranchAddress("ave_col",   &(vars.ave_col)   );
  tree->SetBranchAddress("res_x",     &(vars.res_x)     );
  tree->SetBranchAddress("res_y",     &(vars.res_y)     );
}


