#include "clusterdata.h"
#include <iostream>

cluster::cluster() : hits( new std::vector<hit> ), sumToT(0.), size(0), ave_col(0.0), ave_row(0.0) {}

cluster::cluster(const cluster& c)
  : hits( new std::vector<hit> )
  , sumToT( c.sumToT )
  , size( c.size )
  , ave_col( c.ave_col )
  , ave_row( c.ave_row )
{
  for( auto& hit : *c.hits ) {
    this->hits->emplace_back( hit );
  }
}


cluster::~cluster() {
  hits->clear(); delete hits;
  //std::cout << "cluster::dtor done." << std::endl;
}

cluster& cluster::operator=( const cluster& c ) {
  for( auto& hit : *c.hits ) {
    this->hits->emplace_back( hit );
  }
  this->plane   = c.plane;
  this->sumToT  = c.sumToT;
  this->size    = c.size;
  this->ave_col = c.ave_col;
  this->ave_row = c.ave_row;
  return *this;
};

void cluster::print() const {
  std::cout << "    -> cluster " << this
            << Form(": size = %u, sumToT = %.0f, ave_col = %.2f, ave_row = %.2f", size, sumToT, ave_col, ave_row)
            << std::endl;
}



clusterdata::clusterdata() {
  plane   = new std::vector<int>;
  ave_col = new std::vector<double>;
  ave_row = new std::vector<double>;
  sumToT  = new std::vector<double>;
  size    = new std::vector<int>;
};

clusterdata::~clusterdata() {
  delete size;
  delete sumToT;
  delete ave_row;
  delete ave_col;
  delete plane;
}


