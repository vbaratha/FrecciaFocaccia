#ifndef __HITMAP_DETAILS__
#define __HITMAP_DETAILS__

template<FE type>
HitMap<type>::HitMap(const char* name, const char* title)
  : TH2F( name, title, nCol<type>(), -0.5, nCol<type>()-0.5, nRow<type>(), -0.5, nRow<type>()-0.5)
{
  this->SetContour(99);
}


template<FE type>
TH1F *HitMap<type>::get_distribution(const int& nbins, const double norm)
{
  TH1F *h = new TH1F(Form("%s_dist", this->GetName()), Form(";%s;", "" ), nbins, this->GetMinimum()/norm, this->GetMaximum()/norm );
  for(int xbin=0; xbin<this->GetNbinsX(); xbin++) {
    for(int ybin=0; ybin<this->GetNbinsY(); ybin++) {
      const double& content = this->GetBinContent(xbin+1, ybin+1);
      if( content>0 ) h->Fill( content/norm );
    }}
  
  h->SetFillColor(kGray+1);
  return h;
}


template<FE type>
TH1F *HitMap<type>::get_distribution_log(const int& nbins, const double norm)
{
  TH1F *h = new TH1F(Form("%s_dist_log10", this->GetName()), ";log10;", nbins, -7.0, 1.0 );
  for(int xbin=0; xbin<this->GetNbinsX(); xbin++) {
    for(int ybin=0; ybin<this->GetNbinsY(); ybin++) {
      const double& content = this->GetBinContent(xbin+1, ybin+1);
      if( content>0 ) h->Fill( TMath::Log10(content/norm) );
    }}
  
  h->SetFillColor(kGray+1);
  return h;
}


#endif
