#ifndef __clustering__
#define __clustering__

#include "RawAnalysis.h"
#include "clusterdata.h"

class TH1F;
class TH2F;

class Clustering final : public RawAnalysis {
private:
  TFile *ofile;
  TFile *maskmap_file;
  TTree *otree;
  std::vector<int> out_plane;
  std::vector<double> out_size;
  std::vector<double> out_ave_col;
  std::vector<double> out_ave_row;
  std::vector<double> out_sumToT;
  
  // Holder of hit maps
  std::map<int, TH2F*> maskmaps;
  std::map<unsigned, std::vector<hit>* > hitdata;
  std::map<unsigned, std::vector<cluster*>* > clusterdata;
  std::map<int, TH1F*> dist_nclus;
  std::map<int, TH1F*> dist_size;
  std::map<int, TH1F*> dist_sumToT;
  
  int n_passed;
  
  void clear();
  void print();
  void gen_clusters_col( std::vector<hit>* hits, std::vector<cluster*>* clusters );
  void gen_clusters_row( std::vector<hit>* hits, std::vector<cluster*>* clusters );
  void gen_clusters    ( std::vector<hit>* hits, std::vector<cluster*>* clusters );
  
  void init()         override;
  void processEntry() override;
  void end()          override;

  void worker(const unsigned&, Clustering*);
  
public:
  Clustering( const config& c, const std::string& _filename, const unsigned& num );
  ~Clustering();
};

#endif
