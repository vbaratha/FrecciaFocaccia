#ifndef __SENSOR__
#define __SENSOR__

#include <cmath>
#include <tuple>
#include <set>

#include <TVector2.h>

enum class FE { mimosa26, fei4, fei4_50x50, fei4_100x25 };

// Pixel dimensions
template<FE type> constexpr unsigned nCol() noexcept { return 0; };
template<FE type> constexpr unsigned nRow() noexcept { return 0; };

template<> constexpr unsigned nCol<FE::mimosa26>() noexcept { return 1152; }
template<> constexpr unsigned nRow<FE::mimosa26>() noexcept { return 576; }
  
template<> constexpr unsigned nCol<FE::fei4>() noexcept { return 80; }
template<> constexpr unsigned nRow<FE::fei4>() noexcept { return 336; }
  
template<> constexpr unsigned nCol<FE::fei4_50x50>() noexcept { return 400; }
template<> constexpr unsigned nRow<FE::fei4_50x50>() noexcept { return 336; }
  
template<> constexpr unsigned nCol<FE::fei4_100x25>() noexcept { return 200; }
template<> constexpr unsigned nRow<FE::fei4_100x25>() noexcept { return 672; }

// Column/Row widths
template<FE type> constexpr double col_width() noexcept { return 0; };
template<FE type> constexpr double row_width() noexcept { return 0; };

template<> constexpr double col_width<FE::mimosa26>() noexcept { return 18.5e-3; }
template<> constexpr double row_width<FE::mimosa26>() noexcept { return 18.5e-3; }
  
template<> constexpr double col_width<FE::fei4>() noexcept { return 250.e-3; }
template<> constexpr double row_width<FE::fei4>() noexcept { return 50.e-3; }
  
template<> constexpr double col_width<FE::fei4_50x50>() noexcept { return 50.e-3; }
template<> constexpr double row_width<FE::fei4_50x50>() noexcept { return 50.e-3; }
  
template<> constexpr double col_width<FE::fei4_100x25>() noexcept { return 100.e-3; }
template<> constexpr double row_width<FE::fei4_100x25>() noexcept { return 25.e-3; }

// Pixel dimensions
template<FE type> constexpr unsigned sensor_col                 (const unsigned& fe_col) noexcept { return fe_col; };
template<>        constexpr unsigned sensor_col<FE::fei4_50x50> (const unsigned& fe_col) noexcept { return (0==fe_col%2)? fe_col*5 : fe_col*5 + 4; }
template<>        constexpr unsigned sensor_col<FE::fei4_100x25>(const unsigned& fe_col) noexcept { return (0==fe_col%2)? (fe_col/2)*5 : ((fe_col+1)/2)*5 - 1; }

template<FE type> constexpr unsigned sensor_row                 (const unsigned& fe_row) noexcept { return fe_row; };
template<>        constexpr unsigned sensor_row<FE::fei4_100x25>(const unsigned& fe_row) noexcept { return fe_row*2; }

// readout
template<FE type> constexpr bool is_readout                 (const unsigned& col, const unsigned& row) noexcept { return true; }
template<>        constexpr bool is_readout<FE::fei4_50x50> (const unsigned& col, const unsigned& row) noexcept { return (0==col%10 or 9 ==col%10); }
template<>        constexpr bool is_readout<FE::fei4_100x25>(const unsigned& col, const unsigned& row) noexcept { return ( (0==col%5 or 4==col%5) and ( 0==row%2 ) ); }

// region of interests
template<FE type> constexpr bool is_roi                 (const double& folded_x, const double& folded_y) noexcept { return true; }
template<>        constexpr bool is_roi<FE::fei4_50x50> (const double& folded_x, const double& folded_y) noexcept { return (folded_x > 0.); }
template<>        constexpr bool is_roi<FE::fei4_100x25>(const double& folded_x, const double& folded_y) noexcept { return ( folded_x > 0. and fabs(folded_y) < row_width<FE::fei4_100x25>()/4.0 ); }

// to flip column or not
template<FE type> constexpr bool is_toFlip                 (const unsigned& col) noexcept { return col%2==0; }
template<>        constexpr bool is_toFlip<FE::fei4_50x50> (const unsigned& col) noexcept { return col%10<=5; }
template<>        constexpr bool is_toFlip<FE::fei4_100x25>(const unsigned& col) noexcept { return col%5<=2; }

// Position or errors
namespace SensorGeo {
  template<FE type>
  const TVector2 pos(const double& column, const double& row) noexcept {
    const double& x = ( column - nCol<type>()/2 + 0.5 ) * col_width<type>();
    const double& y = ( row    - nRow<type>()/2 + 0.5 ) * row_width<type>();
    return TVector2( x, y );
  }
  template<FE type>
  const TVector2 unc() noexcept {
    const double& x = col_width<type>()/sqrt(12.0);
    const double& y = row_width<type>()/sqrt(12.0);
    return TVector2( x, y );
  }
  
  enum func { position, uncertainty, ncol, nrow, wcol, wrow, conv_col, conv_row, readout, roi, flip };

  using pos_func     = const TVector2 (*) (const double&, const double&);
  using unc_func     = const TVector2 (*) ();
  using map_func     = unsigned       (*) (const unsigned&);
  using size_func    = unsigned       (*) ();
  using width_func   = double         (*) ();
  using readout_func = bool           (*) (const unsigned&, const unsigned&);
  using roi_func     = bool           (*) (const double&, const double&);
  using flip_func    = bool           (*) (const unsigned&);

  using posfuncs = std::tuple<pos_func, unc_func, size_func, size_func, width_func, width_func, map_func, map_func, readout_func, roi_func, flip_func>;

  template<FE type>
  posfuncs funcs() noexcept {
    return std::make_tuple( &pos<type>, &unc<type>,
                            &nCol<type>, &nRow<type>,
                            &col_width<type>, &row_width<type>,
                            &sensor_col<type>, &sensor_row<type>,
                            &is_readout<type>, &is_roi<type>, &is_toFlip<type> );
  }
};

#endif
