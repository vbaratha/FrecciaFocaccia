#ifndef __correlator__
#define __correlator__

#include "ClusterAnalysis.h"

class Correlator_impl;

class Correlator final : public ClusterAnalysis {
private:

  std::unique_ptr<Correlator_impl> m_impl;
  
  void init()         override;
  void processEntry() override;
  void end()          override;
  
public:
  Correlator( const config& c, const std::string& _filename, const unsigned& num );
  ~Correlator();
};



#endif
