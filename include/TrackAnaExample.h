#ifndef __TrackAnaExample__
#define __TrackAnaExample__

#include "TrackAnalysis.h"
#include "TrackData.h"
#include <TTree.h>
#include <TVector3.h>
#include <TH1F.h>

class TrackAnaExample_impl;

class TrackAnaExample final : public TrackAnalysis {
private:
  
  std::unique_ptr<TrackAnaExample_impl> m_impl;
  
  void init()         override;
  void processEntry() override;
  void end()          override;
  
public:
  TrackAnaExample( const config& c, const std::string& fname, const unsigned& id );
  ~TrackAnaExample();
};

#endif
