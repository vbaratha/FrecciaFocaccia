#ifndef __RAWDATA__
#define __RAWDATA__

#include <vector>

class hit {
public:
  hit(){}
  ~hit(){}
  int iden;
  int col;
  int row;
  int tot;
  int lv1;
};

bool operator<( const hit& h1, const hit& h2 ) noexcept;

class rawdata {
public:
  int euEvt;
  std::vector<int>* iden;
  std::vector<int>* col;
  std::vector<int>* row;
  std::vector<int>* tot;
  std::vector<int>* lv1;
  rawdata();
  ~rawdata();
  void clear();
  hit* get_hit( const unsigned& ihit ) noexcept ;
  hit get_hit_new( const unsigned& ihit ) noexcept;
  const unsigned size() noexcept { return iden->size(); }
private:
  class hit* m_hit;
};

#endif
