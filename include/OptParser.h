#ifndef __OptParser__
#define __OptParser__

#include <string>
#include <vector>
#include <map>
#include <tuple>

class options {
public:
  enum state { idle, cfg, ana, input, help, verbose, batch };
  enum opttag { opt, mode, descr };
private:
  std::vector<std::tuple<std::string, enum state, std::string> > optTags;
  std::map<unsigned, std::string> strings;
public:
  options(const int argc, const char** argv);
  ~options(){}

  void print();
  void print_help();

  template<unsigned N>
  const bool find() const { return strings.find(N) != strings.end(); }
  
  template<unsigned N>
  const std::string& string() const { return strings.at(N); }
  
};

#endif
