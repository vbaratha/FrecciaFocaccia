#ifndef __ClusterAnalysis__
#define __ClusterAnalysis__

#include "IAnalysis.h"
#include "clusterdata.h"
#include "geo.h"

class ClusterAnalysis : public IAnalysis {
public:
  static TVector3 get_global_pos( const cluster& c, geo::prealign do_prealign = geo::prealign::with_prealign, geo::align do_align = geo::align::with_align );
  static TVector2 get_local_pos ( const cluster& c );
  static TVector3 get_global_err( const cluster& c );
  static TVector3 get_local_err ( const cluster& c );
  
protected:
  clusterdata data;
  std::string filename;
  
  void initTree() override;
  
public:
  ClusterAnalysis( const config& c, const std::string& name, const std::string& fname, const unsigned& num );
  virtual ~ClusterAnalysis();
};

#endif
