#ifndef __EudaqConv__
#define __EudaqConv__

#include "IAnalysis.h"
#include "rawdata.h"
#include <TFile.h>
#include <TTree.h>

struct eudaq_data {
  int id_plane;
  int id_hit;
  double id_x;
  double id_y;
  unsigned int id_frame;
  unsigned int id_charge;
  bool id_pivot;
  long long i_time_stamp;
  int i_tlu;
  int i_run;
  int i_event;
};

using eudaq_data = struct eudaq_data;

class EudaqConv final : public IAnalysis {
protected:

  eudaq_data eudata;
  rawdata raw;
  std::string filename;

  int event_number;

  std::unique_ptr<TFile> ofile;
  TTree* otree;
  
  void initTree()     override;
  void init()         override;
  void processEntry() override;
  void end()          override;
  
public:
  EudaqConv( const config& c, const std::string& _filename, const unsigned& num );
  virtual ~EudaqConv();
};


#endif
