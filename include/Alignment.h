#ifndef __Alignment__
#define __Alignment__

#include "ClusterAnalysis.h"
#include "Tracking.h"
#include "alignfitter.h"
#include "track.h"

#include <iostream>
#include <tuple>
#include <algorithm>
#include <memory>

//____________________________________________________________________________________________________
class Alignment final : public Tracking {
 private:
  void do_align(const int nmax=-1);
  void do_align_single( const unsigned& plane, const std::string level, const int& nmax);
  void error_scale_tune( const unsigned&, const int& nmax, bool do_tune=true );
  
  void init()         override;
  //void processEntry() override;
  void end()          override;
  
public:
  Alignment( const config& c, const std::string& _filename, const unsigned& num );
  ~Alignment();
  
};


#endif
