CXX=g++
ROOTFLAGS= $(shell root-config --cflags)
ROOTLIBS= $(shell root-config --libs)

CXXFLAGS= -std=c++11 -O2 -I./include $(ROOTFLAGS)
LIBS += $(ROOTLIBS)

.PHONY: all clean

INC_DIR  := include
HDR_EXTS := h
SRC_EXTS := cc
OBJ_DIR  := src/.objs
BIN_DIR  := bin
TEST_DIR := unit_test

HDRS		:= $(foreach E,$(HDR_EXTS),$(wildcard include/*.$(E)))
HDRS_detail	:= $(foreach E,$(HDR_EXTS),$(wildcard include/details/*.$(E)))
SRCS		:= $(foreach E,$(SRC_EXTS),$(wildcard src/*.$(E)))
OBJS		:= $(foreach E,$(SRC_EXTS),$(patsubst src/%.cc,$(OBJ_DIR)/%.o,$(SRCS)))
BINSRCS		:= $(foreach E,$(SRC_EXTS),$(wildcard main/*.$(E)))
BINS		:= $(foreach E,$(SRC_EXTS),$(patsubst main/%.$(E),$(BIN_DIR)/%,$(BINSRCS)))
TESTSRCS	:= $(foreach E,$(SRC_EXTS),$(wildcard unit_test/*.$(E)))
TESTBINS	:= $(foreach E,$(SRC_EXTS),$(patsubst unit_test/%.$(E),unit_test/%,$(TESTSRCS)))

$(shell mkdir -p $(BIN_DIR))
$(shell mkdir -p $(OBJ_DIR))
$(shell mkdir -p output)
$(shell mkdir -p result)

all: $(BINS) $(TESTBINS) $(OBJS)
	@echo Done.

$(BIN_DIR)/%: main/%.cc $(OBJS) $(HDRS) $(HDRS_detail)
	@echo Compiling $<
	@$(CXX) $(CXXFLAGS) -o $@ $< $(OBJS) $(LIBS)

$(TEST_DIR)/%: $(TEST_DIR)/%.cc $(OBJS) $(HDRS) $(HDRS_detail)
	@echo Compiling $<
	@$(CXX) $(CXXFLAGS) -o $@ $< $(OBJS) $(LIBS)

src/.objs/%.o: src/%.cc $(HDRS) $(HDRS_detail)
	@echo Compiling $<
	@$(CXX) -c -o $@ $< $(CXXFLAGS) 

clean:
	@rm -f $(BINS)
	@rm -f $(OBJS)
	@rm -rf $(BIN_DIR)
	@rm -rf $(OBJ_DIR)
	@echo Cleaning done.

