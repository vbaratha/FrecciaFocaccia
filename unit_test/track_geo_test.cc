#include "geo.h"
#include "track.h"
#include "IAnalysis.h"

#include <iostream>
#include <fstream>

int main( const int argc, const char** argv ) {

  const std::string cfgFile = argv[1];

  IAnalysis::verbosity = IAnalysis::verbose;
  
  std::ifstream config_file( cfgFile );
  config cfg;
  config_file >> cfg;
  config_file.close();

  const unsigned plane = 20;
  
  geo::init( cfg );
  auto& geom = geo::geometry[plane];
  
  geom.pre_align.Set( 0.0, 0.0 );
  geom.Trans.SetXYZ( 0.0, 0.0, 100.0 );
  geom.Rot.SetXYZ( 0.0, 0.0, 0.0 );
  //geom.Rot.SetXYZ( 45*TMath::DegToRad(), 45*TMath::DegToRad(), 0.0 );
  geom.Scale.Set( 0.0, 0.0 );

  track trk;

  trk.x0 = 0.0;
  trk.y0 = 1.0;
  trk.dxdz = 1.e-3;
  trk.dydz = 0.0;

  const TVector3& gpos = trk.get_global( plane );

  std::cout << Form("global position: (%.3f, %.3f, %.3f)",
		    gpos.x(), gpos.y(), gpos.z() )
	    << std::endl;

  const auto& local = geom.global_to_local( gpos );
  std::cout << Form("local: (%.3f, %.3f)", local.X(), local.Y()) << std::endl;
  
  return 0;
}
