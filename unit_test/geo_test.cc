#include "Sensor.h"
#include "geo.h"

#include <TMath.h>
#include <TVector3.h>
#include <iostream>
#include <fstream>

int main( const int argc, const char** argv ) {

  const std::string cfgFile = argv[1];
  const unsigned plane = atoi( argv[2] );
  
  std::ifstream config_file( cfgFile );
  config cfg;
  config_file >> cfg;
  config_file.close();
  
  geo::init( cfg );
  
  auto& geom = geo::geometry[plane];

  geom.pre_align.Set( 2.5, 2.5 );
  geom.Rot.SetXYZ(0.0, 30*TMath::DegToRad(), 60*TMath::DegToRad() );
  geom.Trans.SetXYZ(0.2, 0.7, -0.2);
  
  const TVector2 local(3.27, -1.762);

  int col(-1);
  int row(-1);

  geom.get_pixel( const_cast<TVector2&>(local), col, row );
  
  TVector3 global = geom.local_to_global( local );
  TVector2 local2 = geom.global_to_local( global );
  TVector2 folded = geom.local_to_folded( local, col, row );

  std::cout << "local     : " << local.X() << " " << local.Y() << std::endl;
  std::cout << "global    : " << global.X() << " " << global.Y() << " " << global.Z() << std::endl;
  std::cout << "local'    : " << local2.X() << " " << local2.Y() << std::endl;
  std::cout << "(col, row): " << col << " " << row << std::endl;
  std::cout << "folded    : " << folded.X() << " " << folded.Y() << std::endl;

  std::cout << "err scale : " << geom.global_err().X() << " " << geom.global_err().Y() << std::endl;

  geom.error_scale.Set( 2.0, 2.0 );
  
  std::cout << "err scale : " << geom.global_err().X() << " " << geom.global_err().Y() << std::endl;

  if( (local - local2).Mod() > 1.e-9 ) {
    std::cerr << "\nUnit test failed! (local != local2)" << std::endl;
    return 1;
  }

  std::cerr << "\nUnit test passed!" << std::endl;
  return 0;
}
